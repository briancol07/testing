# Testing 

> This is my path through Testing and QA 

## What is ISTQB? 

* International 
* Software 
* Testing 
* Qualifications 
* Board 
> To Standardize the testing terms 

## Steps 

1. Basic Concepts 
  * Basic Testing terms and definitions 
  * Testing process (Models, types,techniques)
  * Roles and Responsibility of tester  and other team memebers
  * Different tool used for testing 
2. Live Project TestCases
  * Test case writing 
  * Applicable for both manual and automation testing 
  * Application of testing concepts and techniques 
  * Undestand how test case is written in real time
  * Involves  
    * Analysing Requierements
    * Finding test conditions 
    * Test case desing 
  * Automation
    * Learn one scripting language 
      * java
      * Python
      * perk
      * c#
3. Specialization in Tool 
  * hand's on a specific tool 
    * Selenium 
    * Apachi Jmeter 
    * load runner 
4. Become a Certified Tester 
  * ISTQB
  
