# Test Design 

## Objective 

1. Test conditions are elaborated into high-level test cases
2. Identification of defects in the test basis 

## Activities 

1. Designing and pioritizing test cases and set of test cases 
2. Identifying necessary test data to support test conditions and test cases 
3. Designing the test environment and identifying any required infreastructure and tools
4. Capturing bidirectional traceability b/w the test basis , test conditions , test cases 

## Importance 

1. Identification of defects in the test basis 
2. Find defects in test conditions 
3. Find defect is cost Effective 
4. Any time we can trace requiremente , test condition and test cases 



## Example 

Case 1 | Case 2 
-------|-------
1. Switch on power | 1. Switch on power
2. Open door | 2. Close door 
3. Check light ON | 3. Check light OFF 
4. Switch OFF power | 4. Switch OFF power  
