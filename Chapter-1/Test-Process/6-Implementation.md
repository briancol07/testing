## Test implementation 

> Do we now have everything in place to run the tests ?

## Objective 

1. Testware necessary for test execution is created and/or completed 
2. Sequence the test cases into test procedures 
3. Verifying traceability 

## Activities 

1. Developing and prioritizing test procedures and potentially creating automated test scripts 
2. Creating test suites from the test procedures and (if any) automated test scripts 
3. Attanging the test suites within a test execution schedule in a way that results in efficient test Execution  
4. Preparing test data and ensuring it is properly loaded in the test environment 
5. Verifying and updating bidirectional traceability between the test basis, test conditions, test cases , test procedures and test suites   
