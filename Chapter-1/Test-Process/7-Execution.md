# Test Execution 

## Objectives 

> To run test suits according to hte test execution schedule 

## Activities 

1. Executing test either manually or by using test execution tools 
2. Comparing actual results with expected results 
3. Analysing anomalies to establish thei likely causes  
4. Logging the outcome of test execution 
5. Repeating test activities either as a result of actin taken for an anomaly or as part of the planned testing 
6. Verifying and updating bi-directinal traceability between the test basis, test conditions, test cases, test procedures , and test results  
