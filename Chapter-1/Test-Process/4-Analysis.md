# Analysis 

## Objective 

> What to test ? 

1. Test basis is analyzed 
2. Identify testable features 
3. Define test conditions 

## Example 

### Test Basis 

> we need a light system which will glow when door is open and close whe dorr is closed . The system shall work like mentioned 

### Testable Feature 

Light shall glow when door is poen and close when door is closed

### Test Coditions 

1. Light shall glow when door is open 
2. Light shall not glow when door is closed 


## Activities 

1. Analyzing the test basis 
2. Evaluating the test basis 

### Analyzing 

1. Requirement specifications
2. Risk Analysis Report 

### Evaluating 

1. Omission , inconsistencies, inaccuaricies
2. Contradictions in statements
3. Defining and prioritizing test condintions 
4. Bi-directional traceability b/w test basis and test conditions

## Importance 

1. To reduce the likelihood of emitting importance test conditions
2. To Define more Precise and accurate test conditions 
3. To produces test conditions which are to be used as test objectives 
  * Test objectives are traceable to the test  basis 
4. Identification of defect during test analysis 


