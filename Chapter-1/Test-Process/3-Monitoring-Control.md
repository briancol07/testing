# Test Monitoring and control 

## Objective 

1. Compares actual test progress aginst the test 
2. Actions to meet the objectives of test plan 
3. Evaluation of exit criteria 

## Activities 

1. Cheking test results and logs 
2. Assessing the level of component or system quality 
3. Determining if more test are needed 

## Importance 

1. Communication with stakeholders 
2. Deviation from the plan 
3. Desicion to stop testing 



