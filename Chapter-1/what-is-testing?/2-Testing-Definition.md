# Testing Definition 

## Definition 

> The **process** consisting of all lifecycle activities both static and dynamic, concerned with planning , preparation and evaluation of software products and related work product to determine that they satisfy specified requirements, to demostrate that they are fit for purpose and to detec defects.

## First part 

### Activities of testing 

1. All lifecycle activities 
  * As long as the software is active , there is testing 
2. Static and Dynamic 
  * Static without execution SW 
  * Dynamic is executing  SW 
3. Concern with planning and preparation 
  * Testing start 
  * Test Analysis 
  * Test 
  * Test Ends 
  * Software release 
4. Evaluation of SW products 
  * Constanly check if its working correct 

## Second part 

### Objective of testing 

1. To determine that they satify specified requirements 
  * Fullfill all requirements 
2. To demonstrate that they are fit for purpose
  * Work correct all the times 
  * As intended for the customer 
3. Detect defects 
  
