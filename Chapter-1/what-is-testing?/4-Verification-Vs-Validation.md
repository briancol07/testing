# Verification Vs Validation 

## Verification 

1. It does not involve execution of the code 
2. Stactic practice of verifying documents,desing ,code and program 
3. To check whter the software conforms to specifications 
4. Checks " Are we building the product right"?

## Validation

1. It always involves Execution of the code 
2. Dynamic mechanism of validating and testing the actual Product 
3. To check whether software meet the customer Expectations 
4. Checks "Are we building the right product?


## Example 


> Requirement : Windows Shall move up when up button is press and shall move down when down button is pressed 

### Static Testing 

``` 
ButtonPressed(){
  if(Button == UP)
    UpMovement 
  if (Button == Down)
    DownMovement 
}
```
### Dynamic Testing 

Checking if there are movement in the windows .
