# Objective of Testing 

1. Evaluate 
  * Requirements 
  * User stories 
  * Design and code 
2. Verify 
  * Specified requirement fulfilled 
3. Validate 
  * Test objective are fulfilled 
4. Confidence 
  * Increase or maintain the level of quality
5. Prevent 
  * Defect from passing to higher stage 
6. Detect 
  * Failures and defects 
7. Information 
  * To stakeholder
8. Risk 
  * To reduce level of risk 
9. Contract 
  * To comply with legal , Standards 

## Example 

### Requirement 

> For web page , when the login detial is given the next page shall load in few ms and if login details are not correct then show a popup.

## Questions 

1. How much time?
2. Which page will load next?
3. What is the pop up content ? 

## Requirement fulfilled 

1. Login is correct go to next page 
2. Next page shall loaded in 500ms
3. Next page shall contain the personal information 
4. If the login detail is no correct pop up shall appear 
5. Pop up message "password or user-ID is incorrect" 


