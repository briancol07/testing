# Objective is context dependent 

## Component Testing 

1. Find Defect 
  * As many as possible 
2. Increase code Coverage 

## Acceptance Testing 

1. Confirm Requirement fulfilled 
2. Inform Stakeholder 


## Summary 

Component | Acceptance 
----------|-----------
Find Defect | Confirm 
Increase Coverage | Inform 


