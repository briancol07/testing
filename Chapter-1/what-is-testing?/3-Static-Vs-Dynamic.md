# Static Vs Dynamic 

## Static 

* Without the execution
* Performed in Verification Stage 
* Until Detail design (only with documentation)
* Cost Efective 
  * More people and more steps involved

## Dynamic

* With the execution of code 
* Performe in Validation Stage 
* Only on implementation 
* Less cost efective 
## Life cycle 

1. User Requirement 
2. System Requirement
3. Global Requirement
4. Detail Design 
5. Implementation 

## Example 

> Requirement : window shall move up when up button is press and shall move down when down button is pressed.


### Static Testing 

``` 
ButtonPressed(){
  if(Button==Up)
    UpMovement
  else 
    DownMovement
}
```
For example here is a defect for down button so it not fullfil the requirement 

### Dynamic

Testing with the code running and check if all works properly 

-----

## Summary  

Static Testing | Dynamic Testing
---------------|-----------------
Without the Execution of code | With The execution of code 
Performed in Verification Stage | Performed in Validation Stage 
Cost Effective | Less Cost Effective 
Walkthroughs,Code reviews | Functional and non-functional testing 
