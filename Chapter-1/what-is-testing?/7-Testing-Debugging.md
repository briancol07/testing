# Testing and Debugging 

## Testing activities 

* Show failure
* Checks defect fixed 
* Reported 
* Done by Tester 

## Debugging activities 

* Analyse failure 
* Fix Defect
* Debugging is done by developers 


## Tester 

* Observer failure 
* Make report 
* Retest to confirm Fix works 

## Developer 

* Inverstigate failure 
* Isolate defect 
* Fix Defect 
* Check fix works 

