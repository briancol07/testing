# Error - Defect - Failure 

## Error 

> A mistake in coding is called error 

* A human action that produces an incorrect result 
## Defect 

> Error foun by tester is called defect 

* A flaw in a component o system that can cause the component or system to fail to perfms its required function 

## Bug 

> Defect accepted by development team the it is called bug 

* Fault 
  * Defect 
  * Bug 

## Failure 

> Build does not meet the requirements 

* Deviation of the componentn or system from its expected delivery service or result 

## Example

Requirement: If speed is 120 km/h or more "over-speed" warning shall come 

```
if(Speed > 120km/h)
```

* In this example do not accept 120 km/h so its an error 
* if is not found there , later a tester found is a defect 
* If the dev.Accepts its a bug 
* Found after the SW is executed is failure
