# Defect  Root Cause Failure


## Root Cause

> The earliest action or condition that contributed to creating the defects 

## Example 

Requirement : Once the temperatur is more than 115 deg , red light shall glow 

* System test
  * Set temp = 115 
  * Expected = red light off 
  * Measured = Red light on 
* Coding stage 
  * if (temp >= 115)
  * Must be only greater than 
* Requirement 
  * Red ligh shall glow when temp is more than 114
  * So the implementation was incorrect 

## Life cycle 

* Effect 
  * Customer complaints 
  * Failure ( incorrect interest payments calculation)
  * Defect 
    * Single line of incorrect code 
  * Defective code was written 
    * Root cause 
* Action 
  * Product owner training 
  * To solve future problems 

1. Prevent 
2. Reduce 
3. Improve process 

