# Contribution to success 

> Never give direct the Sw to the user 

## Levels of Development Activities 

1. Requirement
2. Design 
3. Coding
4. Software 

## Requirement Level 

1. Requirement reviews 
  * Static Testing 
2. Detect defects in these work Products
3. Reduces the risk of incorrect or unestable functionality being developed
 
## Design level 

1. System Desing 
  * Review it 
  * Without code 
2. Increase each party's understanding 
  * Reduce risk of fundamental Desing defects 

## Coding level 

1. Code under development 
  * Make static test 
  * Increaseeach party's understanding of code 
  * Reduce the risk of defects within the code 

## Software 

1. Verify and validate the Software 
  * Dynamic testing 
  * Detec failure that might otherwise have been missed
  * Removing the defects that causes the failure 
  * The corrected error is sent back to the tester to check 
  * Now in this way. It meets stakeholder needs 

