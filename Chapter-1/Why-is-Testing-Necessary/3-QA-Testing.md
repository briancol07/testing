# Quality assurance (QA) & Testing 

> Qa and testing are not the same but they are related. They are related through **quality management**

* Quality is confirmation to Requirement
* Testing is part of QA 
* Quality Control is a end of phase activitie 


## Quality management 

> Coordinated activities to direct and control an organization with regard to quality 

## QA

> Part of quality management focused on providing confidence that quality requirements will be fulfilled 

* Proper Processes
* Defect Prevention 

## Quality control / testing 

> The operational techniques and activities part of quality management that  are focused on fulfilling quality requirements 


* Test Activities  
* Defect Detection  

