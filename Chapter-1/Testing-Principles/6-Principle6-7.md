# Principle 6 and 7 

## Principle 6 

> Testing is context dependent 

1. Safety-critical industrial control SW is tested differently from an e-commerce mobile app . 
2. Testing in an Agile project is done differently than testing in a sequential lifecycle project  
3. E-commerce (all are different)

## Principle 7 

> Absence of errors is a fallacy 

1. Organizations expect that tester can run all possible tests and find all possible defect
2. Principle 1 & 2 , tell us that this is impossible 
3. Thoroughly testing all specified requirements and fixing all defects found could still produce a system that is difficult to use . 
