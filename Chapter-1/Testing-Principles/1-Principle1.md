# Principle 1 

> Testing shows the presence of defects, not their absence 


1. Testing can show that defects are present 
2. Cannot prove that there are no defects 
3. Reduces the probability of undiscovered defects 

## Example 

20 defects in a SW , you found 5 , but still having defects.
Reduces the probability of undiscoered defects 
