# Principle 4 

> Defect clusters together 

1. A small number of modules contain most of defects 
2. Responsible for most of the operational failures 
3. Helps reduce or eliminate costly changes 


